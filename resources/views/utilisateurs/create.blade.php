@extends('default')

@section('content')

    <h1>Création</h1>

    @include('posts.form')

@endsection
