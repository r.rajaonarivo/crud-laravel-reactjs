@php
    if($post->id){
        $action = ['method' => 'PUT', 'url' => route('posts.update',$post) ];
    } else {
        $action = ['method' => 'POST', 'url' => route('posts.store') ];
    }

@endphp

{!! Form::model($post,$action) !!}
    <div class="form-group">
        {!! Form::label('title', 'Titre') !!}
        {!! Form::text('title', null, ['class' => 'form-control' ]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('slug', 'Slug') !!}
        {!! Form::text('slug', null, ['class' => 'form-control' ]) !!}
    </div>
    <div class="form-group">
        {!! Form::label('slug', 'Slug') !!}
        {!! Form::textarea('content', null, ['class' => 'form-control','rows' => '5' ]) !!}
    </div>
    <div class="form-group">
        <label>
            {!! Form::radio('online', 1) !!}
            En ligne ?
            {!! Form::radio('online', 0) !!}
            Hors ligne ?
        </label>
    </div>

    <button class="btn btn-primary">Modifier</button>
{!! Form::close() !!}
