@extends('default')

@section('content')
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    <div id="root"></div>
@endsection


@section('js')

    <script src="{{ asset('js/app.js') }}" defer></script>

@endsection
