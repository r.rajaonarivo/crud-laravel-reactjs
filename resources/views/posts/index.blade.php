@extends('default')

@section('content')

    <div id="root"></div>
@endsection


@section('js')

    <script src="{{ asset('js/app.js') }}" defer></script>

@endsection
