@extends('default')

@section('title',$title)

@section('content')
    <h1>{{ $title }}</h1>

    <p>
        Je m'appelle RAJAONARIVO Nasolombolatiana Olivier, je suis un développeur et integrateur.
    </p>
    <ul>
        @foreach($numbers as $number)
            <li>{{ $number }}</li>

        @endforeach
    </ul>
@endsection

@section('sidebar')
    <h4>Sidebar</h4>

    <p>A propos de la sidebar</p>

    @parent
@endsection
