import React, { useState, useEffect, Fragment } from "react";
import ReactDOM from 'react-dom'
import UserTable from "./tables/UserTable";
import AddUserForm from "./forms/AddUserForm";
import EditUserForm from "./forms/EditUserForm";

import { useAsyncRequest } from "./hooks";


const App = () => {

    const token = document.querySelector('#_token').value
    const [data, loading] = useAsyncRequest(`ajax/utilisateur/index`);
    // Fixed array of users:
    // const [users, setUsers] = userList;
    const [users, setUsers] = useState(null);

    useEffect(() => {
        if (data) {
        const formattedUsers = data.map((obj, i) => {
            return {
            id: obj.id,
            name: obj.name,
            username: obj.username,
            };
        });
        setUsers(formattedUsers);
        }
    }, [data]);

    const addUser = (user) => {
        user.id = users.length;
        setUsers([...users, user]);
    };


    const deleteUser = (id) => {
        setUsers(users.filter((user) => user.id !== id));
    };
    // Pour connaitre si nous sommes en édition ou en ajout.
    const [editing, setEditing] = useState(false);

    const [urlUpdating, setUrlUpdating] = useState("")

    const [posted, setPosted] = useState("")

    const initialUser = { id: null, name: "", username: "" };

    const [currentUser, setCurrentUser] = useState(initialUser);

    const editUser = (id, user, url) => {
        setEditing(true);
        setCurrentUser(user);
        setUrlUpdating(url)
    };

    const updateUser = (newUser) => {
        setUsers(
        users.map((user) => (user.id === currentUser.id ? newUser : user))
        );
        setCurrentUser(initialUser);
        setEditing(false);
    };

    return (
        <div className="container">
            {posted !== "" && <div className="alert alert-success">
                {posted}
            </div>}
            <h1>React CRUD App with Hooks</h1>
            <div className="row">
                <div className="five columns">
                {editing ? (
                    <div>
                    <h2>Edit user</h2>
                    <EditUserForm
                        currentUser={currentUser}
                        setEditing={setEditing}
                        updateUser={updateUser}
                        urlUpdating={urlUpdating}
                        token={token}
                        setPosted={setPosted}
                    />
                    </div>
                ) : (
                    <div>
                        <h2>Add user</h2>
                        <AddUserForm
                            addUser={addUser}
                            token={token}
                            setPosted={setPosted}
                        />
                    </div>
                )}
                </div>
                {loading || !users ? (
                <p>Loading...</p>
                ) : (
                <div className="seven columns">
                    <h2>View users</h2>

                    <UserTable
                    users={users}
                    deleteUser={deleteUser}
                    editUser={editUser}
                    token={token}
                    setPosted={setPosted}
                    />
                </div>
                )}
            </div>
        </div>
    );
};

export default App;



if (document.querySelector('#root')) {
    ReactDOM.render(<App/>, document.querySelector('#root'))
}
