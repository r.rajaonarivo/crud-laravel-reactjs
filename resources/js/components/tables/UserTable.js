import React from 'react';
import AsyncRequestPost from './AsyncRequestPost'

const UserTable = (props) => {
    const handleFormSubmit = e => {
        e.preventDefault()
        props.deleteUser(parseInt(e.target.identifiant.value))
        if(AsyncRequestPost(e.target,"utilisateurs/" + e.target.identifiant.value)){
            props.setPosted('Suppression faite avec succès')
        }
    }

    return (
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                { props.users.length > 0 ? (
                    props.users.map(user => {
                        const {id, name, username} = user;
                        return (
                            <tr key={id}>
                                <td>{id}</td>
                                <td>{name}</td>
                                <td>{username}</td>
                                <td>
                                    <form onSubmit={handleFormSubmit}>
                                        <input type="hidden" name="_method" id="_method" value="DELETE"/>
                                        <input type="hidden" name="_token" id="_token" value={props.token}/>
                                        <input type="hidden" name="identifiant" id="identifiant" value={id}/>
                                        <button >Delete</button>
                                    </form>

                                    <button onClick={() => props.editUser(id, user, "utilisateurs/" + id)}>Edit</button>
                                </td>
                            </tr>
                        )
                    })
                ) : (
                    <tr>
                        <td colSpan={4}>No users found</td>
                    </tr>
                )
                }
            </tbody>
        </table>
    )
}

export default UserTable;
