import { useEffect, useState } from 'react';

const useAsyncRequest = (url) => {
    const [data,setData] = useState(null)
    const [loading,setLoading] = useState(false)

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url)
            const responseData = await response.json()
            if (response.ok) {
                setData(responseData)
                setLoading(false)
            } else {
                console.warn("Something went wrong fetching the API...", err);
                setLoading(false);
            }
        }

        if (url) {
            fetchData(url)
        }

    }, [url]);

    return [data,loading]
}

export default useAsyncRequest;
