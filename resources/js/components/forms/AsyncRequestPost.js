import React from 'react'

function formToJSON( elem ) {
    let output = {};
    new FormData( elem ).forEach(
      ( value, key ) => {
        // Check if property already exist
        if ( Object.prototype.hasOwnProperty.call( output, key ) ) {
          let current = output[ key ];
          if ( !Array.isArray( current ) ) {
            // If it's not an array, convert it to an array.
            current = output[ key ] = [ current ];
          }
          current.push( value ); // Add the new value to the array.
        } else {
          output[ key ] = value;
        }
      }
    );
    return JSON.stringify( output );
}

const AsyncRequestPost = (form,url) => {
    var data = false

    const options = {
        method: 'POST',
        body: formToJSON(form),
        headers: {
            'Content-Type':'application/json'
        }
    }

    const fetchData = async () => {
        const response = await fetch(url,options)
        const responseData = await response.json()
        if (response.ok) {
            data = responseData

        } else {
            console.warn("Something went wrong fetching the API...", err);

        }
    }

    fetchData(url,options)

    return true
}




export default AsyncRequestPost;

