import React, {useState,Fragment} from 'react';
import AsyncRequestPost from './AsyncRequestPost'


const AddUserForm = (props) => {

    const initUser = {id: null, name: '', username: ''};

    const [user, setUser] = useState(initUser);

    const handleChange = e => {
        const {name, value} = e.target;
        setUser({...user, [name]: value});
    }

    const handleSubmit = e => {

        if (user.name && user.username) {
            handleChange(e, props.addUser(user));

        }
    }

    const handleFormSubmit = e => {
        e.preventDefault()
        //setPosted(AsyncRequestPost(e.target,"utilisateurs"))
        if(AsyncRequestPost(e.target,"utilisateurs")) {
            props.setPosted('Création faite avec succès.')
        }
    }

    return (
        <Fragment>
            <form onSubmit={handleFormSubmit}>
                <input type="hidden" name="_token" id="_token" value={props.token}/>
                <label>Name</label>
                <input className="u-full-width" type="text" value={user.name} name="name" onChange={handleChange} />
                <label>Username</label>
                <input className="u-full-width" type="text" value={user.username} name="username" onChange={handleChange} />
                <button className="button-primary" type="submit" onClick={handleSubmit} >Add user</button>
            </form>
        </Fragment>
    )
}

export default AddUserForm;
