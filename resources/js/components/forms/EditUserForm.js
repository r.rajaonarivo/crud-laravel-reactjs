import React, {useState, useEffect, Fragment} from 'react';
import AsyncRequestPost from './AsyncRequestPost'

const EditUserForm = (props) => {

    useEffect(() => {
        setUser(props.currentUser)
    }, [props])

    const [user, setUser] = useState(props.currentUser);

    const handleChange = e => {
        const {name, value} = e.target;
        setUser({...user, [name]: value});
        }

    const handleSubmit = e => {

    }

    const handleFormSubmit = e => {
        e.preventDefault()
        if (user.name && user.username) props.updateUser(user);
        if(AsyncRequestPost(e.target,props.urlUpdating)){
            props.setPosted('Modification faite avec succès')
        }
    }

    return (
        <Fragment>
            <form onSubmit={handleFormSubmit}>
                <input type="hidden" name="_method" id="_method" value="PUT"/>
                <input type="hidden" name="_token" id="_token" value={props.token}/>
                <label>Name</label>
                <input className="u-full-width" type="text" value={user.name} name="name" onChange={handleChange} />
                <label>Username</label>
                <input className="u-full-width" type="text" value={user.username} name="username" onChange={handleChange} />
                <button className="button-primary" type="submit" onClick={handleSubmit} >Edit user</button>
                <button type="submit" onClick={() => props.setEditing(false)} >Cancel</button>
            </form>
        </Fragment>

    )
}

export default EditUserForm;
