<?php

use Illuminate\Support\Facades\Route;



Route::resource('posts', 'PostsController');
Route::resource('utilisateurs','UtilisateurController');
Route::get('ajax/utilisateur/index','UtilisateurController@ajaxIndex');

// Ajax
Route::get('postsOnline/{key}','PostsController@postsOnline')->name('postsOnline');

Route::get('a-propos','PagesController@about')->name('about');
Route::get('/', 'WelcomeController@index');
