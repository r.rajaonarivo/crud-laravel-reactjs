<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title','slug','content','online'];

    function scopePublished($query){
        return $query->where('online',true);
    }

    function scopeNoPublished($query){
        return $query->where('online',false);
    }
}
