<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Ip
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->ip() !== '127.0.0.1'){
            return $next($request);
        }
        return response('Unauthorized.',401);
    }
}
