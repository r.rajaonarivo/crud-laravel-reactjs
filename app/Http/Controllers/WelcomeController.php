<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;



class WelcomeController extends Controller{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(){

        return view('welcome');
    }
}
