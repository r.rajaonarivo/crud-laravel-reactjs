<?php

namespace App\Http\Controllers;

use App\utilisateur;
use Illuminate\Http\Request;

class UtilisateurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('utilisateurs.index');
    }

    public function ajaxIndex(){
        $utilisateurs = utilisateur::get();
        return $utilisateurs->tojson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = utilisateur::create($request->all());
        return $post->tojson();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function show(utilisateur $utilisateur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function edit(utilisateur $utilisateur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, utilisateur $utilisateur)
    {
        $utilisateur->update($request->all());
        return $utilisateur->tojson();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function destroy(utilisateur $utilisateur)
    {
        $utilisateur->delete();
        $utilisateur = null;
        return true;
    }
}
