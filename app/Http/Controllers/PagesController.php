<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function about(){
        $title = 'A propos';
        for ($i=1;$i<=9;$i++){
            $numbers[] = $i;
        }
        return view('pages.about',compact('title','numbers'));
    }
}
